// Copyright Martin Vejnar 2012.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#include "mm_solver.hpp"

#include <ida/ida.h>
#include <ida/ida_dense.h>
#include <nvector/nvector_serial.h>

struct mm_solver::expr_evaluator
	: vams::default_visitor<mm_solver::expr_evaluator, vams::vpi_value>
{
	expr_evaluator(mm_solver & solver, merged_module const & mm, realtype * s, realtype * ddts, realtype t)
		: solver(solver), mm(mm), s(s), ddts(ddts), t(t)
	{
	}

	vams::vpi_value operator()(vams::vpi_constant * ee)
	{
		return ee->value;
	}

	vams::vpi_value operator()(vams::vpi_unary_expr * ee)
	{
		// XXX
		return vams::visit_expr(*this, ee->arg.get());
	}

	vams::vpi_value operator()(vams::vpi_ddt_expr * ee)
	{
		return ddts[solver.m_ddt_var_map[ee]];
	}

	vams::vpi_value operator()(vams::vpi_binary_expr * ee)
	{
		// XXX
		vams::vpi_value lhs = vams::visit_expr(*this, ee->lhs.get());
		vams::vpi_value rhs = vams::visit_expr(*this, ee->rhs.get());

		double lhs_val = lhs.get_real();
		double rhs_val = rhs.get_real();

		switch (ee->op)
		{
		case vams::vpi_binary_expr::op_add:
			return lhs_val + rhs_val;
		case vams::vpi_binary_expr::op_sub:
			return lhs_val - rhs_val;
		case vams::vpi_binary_expr::op_mul:
			return lhs_val * rhs_val;
		case vams::vpi_binary_expr::op_div:
			return lhs_val / rhs_val;
		case vams::vpi_binary_expr::op_mod:
			return std::fmod(lhs_val, rhs_val);
		default:
			throw std::runtime_error("Unknown binary operator.");
		}
	}

	vams::vpi_value operator()(vams::vpi_cond_expr * ee)
	{
		// XXX
		return vams::visit_expr(*this, ee->lhs.get());
	}

	vams::vpi_value operator()(vams::vpi_paren_expr * ee)
	{
		return vams::visit_expr(*this, ee->nested.get());
	}

	vams::vpi_value operator()(vams::vpi_function_call_expr * ee)
	{
		if (ee->signal_access)
		{
			double res;
			if (ee->signal_access->type == vams::vpi_access_function_call::at_potential)
			{
				res = s[mm.get_net_index(ee->signal_access->branch->lhs_net)]
					- s[mm.get_net_index(ee->signal_access->branch->rhs_net)];
			}
			else
			{
				std::size_t r = mm.get_branch_index(ee->signal_access->branch);
				res = s[r];
			}

			if (ee->signal_access->reverse_access)
				res = -res;
			return res;
		}
		else
		{
			if (ee->function_name == "sin")
			{
				if (ee->arguments.size() != 1)
					throw std::runtime_error("Expected exactly one arguments for `sin`.");
				vams::vpi_value arg = vams::visit_expr(*this, ee->arguments[0].get());

				double argd;
				if (arg.kind() == vams::vpi_value::k_int)
					argd = arg.get_int();
				else if (arg.kind() == vams::vpi_value::k_real)
					argd = arg.get_real();
				else
					throw std::runtime_error("The argument to `sin` must be numerical.");

				return std::sin(argd);
			}
		}

		throw std::runtime_error("Unknown function: " + ee->function_name);
	}

	vams::vpi_value operator()(vams::vpi_port_probe_expr * ee)
	{
		throw std::runtime_error("Not implemented yet.");
	}

	vams::vpi_value operator()(vams::vpi_decl_ref_expr * ee)
	{
		if (ee->decl_name == "$abstime")
		{
			return t;
		}

		throw std::runtime_error("Can't handle `" + ee->decl_name + "` in an expression.");
	}

	mm_solver & solver;
	merged_module const & mm;
	realtype * s;
	realtype * ddts;
	realtype t;
};

struct mm_solver::stmt_walker
	: vams::default_visitor<mm_solver::stmt_walker>
{
	stmt_walker(mm_solver & solver, merged_module const & mm, realtype * r, realtype * s, realtype * ddts, realtype t)
		: solver(solver), mm(mm), r(r), s(s), ddts(ddts), t(t)
	{
	}

	return_type operator()(vams::vpi_contribution_stmt * ee)
	{
		expr_evaluator eval(solver, mm, s, ddts, t);
		vams::vpi_value vpires = vams::visit_expr(eval, ee->rhs.get());
		assert(vpires.kind() == vams::vpi_value::k_real || vpires.kind() == vams::vpi_value::k_int);

		double res = 0;
		if (vpires.kind() == vams::vpi_value::k_real)
			res = vpires.get_real();
		else
			res = vpires.get_int();

		vams::vpi_function_call_expr * fncall = dynamic_cast<vams::vpi_function_call_expr *>(ee->lhs.get());
		assert(fncall);
		assert(fncall->signal_access);

		std::size_t bi = mm.get_branch_index(fncall->signal_access->branch);
		r[bi] += fncall->signal_access->reverse_access? -res: res;
	}

	using default_visitor::operator();

	mm_solver & solver;
	merged_module const & mm;
	realtype * r;
	realtype * s;
	realtype * ddts;
	realtype t;
};

int mm_solver::system_fn(realtype tt, N_Vector yy, N_Vector yp, N_Vector rr, void *user_data)
{
	realtype * r = NV_DATA_S(rr);
	realtype * s = NV_DATA_S(yy);
	realtype * ddts = NV_DATA_S(yp);

	mm_solver & solver = *(mm_solver *)user_data;
	merged_module const & mm = solver.mm;

	// non-ground nets
	for (std::size_t i = 0; i < mm.nets.size(); ++i)
		r[i] = 0;

	for (std::map<vams::vpi_branch *, std::size_t>::const_iterator it = mm.branch_indexes.begin(); it != mm.branch_indexes.end(); ++it)
	{
		r[mm.get_net_index(it->first->lhs_net)] += s[it->second];
		r[mm.get_net_index(it->first->rhs_net)] -= s[it->second];
	}

	// the ground net
	r[mm.get_net_index(mm.ground_net)] = s[mm.get_net_index(mm.ground_net)];

	// branch currents
	for (std::map<vams::vpi_branch *, std::size_t>::const_iterator it = mm.branch_indexes.begin(); it != mm.branch_indexes.end(); ++it)
	{
		if (mm.branch_kinds.find(it->first)->second == vams::vpi_access_function_call::at_potential)
		{
			r[it->second] = s[mm.get_net_index(it->first->rhs_net)] - s[mm.get_net_index(it->first->lhs_net)];
		}
		else
		{
			r[it->second] = -s[it->second];
		}
	}

	{
		stmt_walker walker(solver, mm, r, s, ddts, tt);
		for (size_t i = 0; i < mm.stmts.size(); ++i)
			vams::visit_stmt(walker, mm.stmts[i].get());
	}

	// ddt variables
	{
		expr_evaluator eval(solver, mm, s, ddts, tt);
		for (std::map<vams::vpi_ddt_expr *, std::size_t>::const_iterator it = solver.m_ddt_var_map.begin(); it != solver.m_ddt_var_map.end(); ++it)
			r[it->second] = s[it->second] - vams::visit_expr(eval, it->first->nested.get()).get_real();
	}

	return 0;
}

struct ddt_locator
	: vams::default_visitor<ddt_locator>
{
	ddt_locator(std::size_t next_var, std::vector<vams::vpi_ddt_expr *> & ddt_vars, std::map<vams::vpi_ddt_expr *, std::size_t> & ddt_var_map)
		: m_next_var(next_var), m_ddt_var_map(ddt_var_map), m_ddt_vars(ddt_vars)
	{
	}

	void operator()(vams::vpi_ddt_expr * ee)
	{
		m_ddt_var_map[ee] = m_next_var;
		m_ddt_vars.push_back(ee);
		++m_next_var;
		vams::visit_expr(*this, ee->nested.get());
	}

	using default_visitor::operator();

	std::size_t m_next_var;
	std::map<vams::vpi_ddt_expr *, std::size_t> & m_ddt_var_map;
	std::vector<vams::vpi_ddt_expr *> & m_ddt_vars;
};

mm_solver::mm_solver(merged_module const & mm)
	: mm(mm)
{
	if (mm.ground_net == 0)
		throw std::runtime_error("No ground node? Really?");

	N = mm.nets.size() + mm.branch_indexes.size();

	{
		ddt_locator visitor(N, m_ddt_vars, m_ddt_var_map);
		for (std::size_t i = 0; i < mm.stmts.size(); ++i)
			vams::visit_stmt(visitor, mm.stmts[i].get());
	}

	N += m_ddt_vars.size();

	ida_mem = IDACreate();

	y = N_VNew_Serial(N);
	ddty = N_VNew_Serial(N);
	std::fill(NV_DATA_S(y), NV_DATA_S(y) + N, 0.0);
	std::fill(NV_DATA_S(ddty), NV_DATA_S(ddty) + N, 0.0);

	int res = IDAInit(ida_mem, &system_fn, 0, y, ddty);
	IDASetUserData(ida_mem, this);
	IDASStolerances(ida_mem, 1e-6, 1e-6);
	res = IDADense(ida_mem, N);

	{
		N_Vector id = N_VNew_Serial(N);
		std::fill(NV_DATA_S(id), NV_DATA_S(id) + N - m_ddt_var_map.size(), 0.0);
		std::fill(NV_DATA_S(id) + N - m_ddt_var_map.size(), NV_DATA_S(id) + N, 1.0);
		IDASetId(ida_mem, id);
		N_VDestroy_Serial(id);
	}

	res = IDACalcIC(ida_mem, IDA_YA_YDP_INIT, 1e-6);
	res = IDAGetConsistentIC(ida_mem, y, ddty);
}

std::vector<double> mm_solver::get_iter(double t)
{
	std::vector<double> r(NV_DATA_S(y), NV_DATA_S(y) + N);
	r.insert(r.end(), NV_DATA_S(ddty) + N - m_ddt_var_map.size(), NV_DATA_S(ddty) + N);

	double tt;
	IDASolve(ida_mem, t, &tt, y, ddty, IDA_NORMAL);

	return r;
}

mm_solver::~mm_solver()
{
	N_VDestroy_Serial(y);
	N_VDestroy_Serial(ddty);
	IDAFree(&ida_mem);
}
