// Copyright Martin Vejnar 2012.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#include "vams_sema.hpp"
#include <set>

static vams::vpi_net * make_net(vams::vpi_module & m, std::string const & name)
{
	vams::scope_item_t & item = m.scope[name];
	if (item.kind != vams::si_none && item.kind != vams::si_net)
		throw std::runtime_error("Can't use the name " + name + " as a net identifier.");

	std::shared_ptr<vams::vpi_net> & n = m.nets[name];
	if (!n)
	{
		n.reset(new vams::vpi_net());
		n->name = name;
		item = n.get();
	}
	return n.get();
}

static vams::vpi_branch * make_unnamed_branch(vams::vpi_module & m, vams::vpi_net * lhs_net, vams::vpi_net * rhs_net = 0)
{
	if (std::less<vams::vpi_net *>()(lhs_net, rhs_net))
		std::swap(lhs_net, rhs_net);

	std::shared_ptr<vams::vpi_branch> & b = m.unnamed_branches[std::make_pair(lhs_net, rhs_net)];
	if (!b)
	{
		b.reset(new vams::vpi_branch());
		b->lhs_net = lhs_net;
		b->rhs_net = rhs_net;

		// XXX: check disciplines...
	}

	return b.get();
}

static void resolve_decls(vams::vpi_module & m, vams::vpi_expr * e, bool create_implicit_nets);
static void resolve_decls(vams::vpi_module & m, vams::vpi_stmt * e, bool create_implicit_nets);

struct decl_resolver
{
	typedef void return_type;

	bool create_implicit_nets;

	decl_resolver(vams::vpi_module & m, bool create_implicit_nets)
		: m(m), create_implicit_nets(create_implicit_nets)
	{
	}

	void operator()(vams::vpi_decl_ref_expr * ee)
	{
		if (create_implicit_nets)
		{
			vams::scope_item_t & item = m.scope[ee->decl_name];
			if (item.kind == vams::si_none)
				item = make_net(m, ee->decl_name);
			ee->item = item;
		}
		else
		{
			std::map<std::string, vams::scope_item_t>::const_iterator it = m.scope.find(ee->decl_name);
			if (it != m.scope.end())
				ee->item = it->second;
		}
	}

	void operator()(vams::vpi_function_call_expr * ee)
	{
		for (std::size_t i = 0; i < ee->arguments.size(); ++i)
		{
			resolve_decls(m, ee->arguments[i].get(), create_implicit_nets);
		}

		// Check if this is in fact an access function call
		std::shared_ptr<vams::vpi_access_function_call> & access = ee->signal_access;

		vams::vpi_branch * access_branch = 0;
		if (ee->arguments.size() == 1)
		{
			vams::vpi_decl_ref_expr * arg = dynamic_cast<vams::vpi_decl_ref_expr *>(ee->arguments[0].get());
			if (arg)
			{
				if (arg->item.kind == vams::si_branch)
				{
					vams::vpi_branch * b = arg->item.branch;
					if (b->discipline && b->discipline->potential && b->discipline->potential->access == ee->function_name)
					{
						access.reset(new vams::vpi_access_function_call());
						access->branch = b;
						access->discipline = b->discipline;
						access->nature = b->discipline->potential;
						access->type = vams::vpi_access_function_call::at_potential;
						access->reverse_access = false;
					}
					else if (b->discipline && b->discipline->flow && b->discipline->flow->access == ee->function_name)
					{
						access.reset(new vams::vpi_access_function_call());
						access->branch = b;
						access->discipline = b->discipline;
						access->nature = b->discipline->flow;
						access->type = vams::vpi_access_function_call::at_flow;
						access->reverse_access = false;
					}
				}

				if (arg->item.kind == vams::si_net)
				{
					vams::vpi_net * lhs_net = arg->item.net;
					if (lhs_net->discipline && lhs_net->discipline->potential && lhs_net->discipline->potential->access == ee->function_name)
					{
						access.reset(new vams::vpi_access_function_call());
						access->branch = make_unnamed_branch(m, lhs_net);
						access->discipline = lhs_net->discipline;
						access->nature = lhs_net->discipline->potential;
						access->type = vams::vpi_access_function_call::at_potential;
						access->reverse_access = access->branch->lhs_net != lhs_net;
					}
					else if (lhs_net->discipline && lhs_net->discipline->flow && lhs_net->discipline->flow->access == ee->function_name)
					{
						access.reset(new vams::vpi_access_function_call());
						access->branch = make_unnamed_branch(m, lhs_net);
						access->discipline = lhs_net->discipline;
						access->nature = lhs_net->discipline->flow;
						access->type = vams::vpi_access_function_call::at_flow;
						access->reverse_access = access->branch->lhs_net != lhs_net;
					}
				}
			}
		}

		if (ee->arguments.size() == 2)
		{
			vams::vpi_decl_ref_expr * lhs = dynamic_cast<vams::vpi_decl_ref_expr *>(ee->arguments[0].get());
			vams::vpi_decl_ref_expr * rhs = dynamic_cast<vams::vpi_decl_ref_expr *>(ee->arguments[1].get());

			if (lhs && rhs && lhs->item.kind == vams::si_net && rhs->item.kind == vams::si_net)
			{
				vams::vpi_net * lhs_net = lhs->item.net;
				vams::vpi_net * rhs_net = rhs->item.net;

				vams::vpi_discipline * disc = lhs_net->discipline;
				if (disc->flow && disc->flow->access == ee->function_name)
				{
					access.reset(new vams::vpi_access_function_call());
					access->type = vams::vpi_access_function_call::at_flow;
					access->nature = disc->flow;
					access->discipline = disc;
					access->branch = make_unnamed_branch(m, lhs_net, rhs_net);
					access->reverse_access = access->branch->lhs_net != lhs_net;
				}
				else if (disc->potential && disc->potential->access == ee->function_name)
				{
					access.reset(new vams::vpi_access_function_call());
					access->type = vams::vpi_access_function_call::at_potential;
					access->nature = disc->potential;
					access->discipline = disc;
					access->branch = make_unnamed_branch(m, lhs_net, rhs_net);
					access->reverse_access = access->branch->lhs_net != lhs_net;
				}
			}
		}
	}

	void operator()(vams::vpi_paren_expr * ee)
	{
		resolve_decls(m, ee->nested.get(), create_implicit_nets);
	}

	void operator()(vams::vpi_ddt_expr * ee)
	{
		resolve_decls(m, ee->nested.get(), create_implicit_nets);
	}

	void operator()(vams::vpi_cond_expr * ee)
	{
		resolve_decls(m, ee->cond.get(), create_implicit_nets);
		resolve_decls(m, ee->lhs.get(), create_implicit_nets);
		resolve_decls(m, ee->rhs.get(), create_implicit_nets);
	}

	void operator()(vams::vpi_unary_expr * ee)
	{
		resolve_decls(m, ee->arg.get(), create_implicit_nets);
	}

	void operator()(vams::vpi_binary_expr * ee)
	{
		resolve_decls(m, ee->lhs.get(), create_implicit_nets);
		resolve_decls(m, ee->rhs.get(), create_implicit_nets);
	}

	void operator()(vams::vpi_constant * ee)
	{
	}

	void operator()(vams::vpi_port_probe_expr * ee)
	{
	}

	void operator()(vams::vpi_null_stmt * ee)
	{
	}

	void operator()(vams::vpi_analog_stmt * ee)
	{
		resolve_decls(m, ee->nested.get(), create_implicit_nets);
	}

	void operator()(vams::vpi_compound_stmt * ee)
	{
		for (size_t i = 0; i < ee->stmts.size(); ++i)
			resolve_decls(m, ee->stmts[i].get(), create_implicit_nets);
	}

	void operator()(vams::vpi_contribution_stmt * ee)
	{
		resolve_decls(m, ee->lhs.get(), create_implicit_nets);
		resolve_decls(m, ee->rhs.get(), create_implicit_nets);
	}

	vams::vpi_module & m;
};

static void resolve_decls(vams::vpi_module & m, vams::vpi_expr * e, bool create_implicit_nets)
{
	decl_resolver visitor(m, create_implicit_nets);
	vams::visit_expr(visitor, e);
}

static void resolve_decls(vams::vpi_module & m, vams::vpi_stmt * s, bool create_implicit_nets)
{
	decl_resolver visitor(m, create_implicit_nets);
	vams::visit_stmt(visitor, s);
}

template <typename Node, typename Edge>
struct graph
{
	struct node;
	typedef std::size_t node_descriptor;

	struct edge
	{
		Edge label;
		node_descriptor source;
		node_descriptor target;
	};
	typedef std::size_t edge_descriptor;

	struct node
	{
		Node label;
		std::set<edge_descriptor> outedges;
		std::set<edge_descriptor> inedges;
	};

	std::vector<node> m_nodes;
	std::vector<edge> m_edges;

	node_descriptor add_node(Node const & label)
	{
		node n = { label };
		m_nodes.push_back(n);
		return m_nodes.size() - 1;
	}

	edge_descriptor add_edge(node_descriptor source, node_descriptor target, Edge label)
	{
		edge e = { label, source, target };
		m_edges.push_back(e);

		edge_descriptor r = m_edges.size() - 1;
		m_nodes[source].outedges.insert(r);
		m_nodes[target].inedges.insert(r);
		return r;
	}
};

static graph<vams::vpi_module *, int> build_module_dependency_graph(vams::vpi_root & r)
{
	graph<vams::vpi_module *, int> g;

	std::map<vams::vpi_module *, std::size_t> module_node_mapping;
	for (std::map<std::string, std::shared_ptr<vams::vpi_module> >::iterator it = r.modules.begin(); it != r.modules.end(); ++it)
	{
		module_node_mapping[it->second.get()] = g.add_node(it->second.get());
	}

	for (std::map<std::string, std::shared_ptr<vams::vpi_module> >::iterator it = r.modules.begin(); it != r.modules.end(); ++it)
	{
		vams::vpi_module & m = *it->second;
		std::size_t mn = module_node_mapping[&m];

		for (std::size_t j = 0; j < m.module_instances.size(); ++j)
		{
			g.add_edge(mn, module_node_mapping[m.module_instances[j].module], 0);
		}
	}

	return g;
}

static std::set<vams::vpi_module *> get_top_level_modules(vams::vpi_root & r)
{
	std::set<vams::vpi_module *> res;
	graph<vams::vpi_module *, int> dg = build_module_dependency_graph(r);

	for (std::size_t i = 0; i < dg.m_nodes.size(); ++i)
	{
		if (dg.m_nodes[i].inedges.empty())
			res.insert(dg.m_nodes[i].label);
	}

	return res;
}

struct contribution_lhs_checker
	: vams::default_visitor<contribution_lhs_checker>
{
	void operator()(vams::vpi_contribution_stmt * ee)
	{
		vams::vpi_function_call_expr * fncall = dynamic_cast<vams::vpi_function_call_expr *>(ee->lhs.get());
		if (!fncall || !fncall->signal_access)
			throw std::runtime_error("Left-hand side of a contribution statement must be a signal access.");
	}

	using default_visitor::operator();
};

void vams_sema(vams::vpi_root & r)
{
	// Resolve natures for disciplines
	for (std::map<std::string, vams::vpi_discipline>::iterator it = r.disciplines.begin(); it != r.disciplines.end(); ++it)
	{
		if (!it->second.potential_name.empty())
		{
			std::map<std::string, vams::vpi_nature>::iterator nat_it = r.natures.find(it->second.potential_name);
			if (nat_it == r.natures.end())
				throw std::runtime_error("Unknown nature: " + it->second.potential_name);
			it->second.potential = &nat_it->second;
		}

		if (!it->second.flow_name.empty())
		{
			std::map<std::string, vams::vpi_nature>::iterator nat_it = r.natures.find(it->second.flow_name);
			if (nat_it == r.natures.end())
				throw std::runtime_error("Unknown nature: " + it->second.flow_name);
			it->second.flow = &nat_it->second;
		}
	}

	for (std::map<std::string, std::shared_ptr<vams::vpi_module> >::iterator it = r.modules.begin(); it != r.modules.end(); ++it)
	{
		vams::vpi_module & m = *it->second;

		// Build scope for the module
		{
			for (std::map<std::string, std::shared_ptr<vams::vpi_net> >::iterator it = m.nets.begin(); it != m.nets.end(); ++it)
			{
				vams::scope_item_t & item = m.scope[it->first];
				if (item.kind != vams::si_none)
					throw std::runtime_error("The name " + it->first + " is already declared.");
				item = it->second.get();
			}

			for (std::map<std::string, std::shared_ptr<vams::vpi_branch> >::iterator it = m.named_branches.begin(); it != m.named_branches.end(); ++it)
			{
				vams::scope_item_t & item = m.scope[it->first];
				if (item.kind != vams::si_none)
					throw std::runtime_error("The name " + it->first + " is already declared.");
				item = it->second.get();
			}

			for (size_t i = 0; i < m.module_parameters.size(); ++i)
			{
				vams::vpi_parameter & param = m.module_parameters[i];
				vams::scope_item_t & item = m.scope[param.name];
				if (item.kind != vams::si_none)
					throw std::runtime_error("The name " + param.name + " is already declared.");
				item = &param;
			}
		}

		// Resolve nets in branches
		{
			for (std::map<std::string, std::shared_ptr<vams::vpi_branch> >::iterator it = m.named_branches.begin(); it != m.named_branches.end(); ++it)
			{
				std::shared_ptr<vams::vpi_branch> & b = it->second;
				vams::scope_item_t item = m.lookup(b->lhs_net_name);
				if (item.kind != vams::si_net)
					throw std::runtime_error("Failed to resolve name " + b->lhs_net_name + " to a net.");
				b->lhs_net = item.net;

				if (!b->rhs_net_name.empty())
				{
					item = m.lookup(b->rhs_net_name);
					if (item.kind != vams::si_net)
						throw std::runtime_error("Failed to resolve name " + b->rhs_net_name + " to a net.");
					b->rhs_net = item.net;
				}

				// XXX: check that the nets are compatible and assign discipline
			}
		}

		for (std::size_t i = 0; i < m.ports.size(); ++i)
			m.ports[i].net = make_net(m, m.ports[i].name);

		for (std::map<std::string, std::shared_ptr<vams::vpi_net> >::iterator net_it = m.nets.begin(); net_it != m.nets.end(); ++net_it)
		{
			if (net_it->second->discipline_name.empty())
				continue;

			std::map<std::string, vams::vpi_discipline>::iterator ref_it = r.disciplines.find(net_it->second->discipline_name);
			if (ref_it == r.disciplines.end())
				throw std::runtime_error("Unknown discipline: " + net_it->second->discipline_name);
			net_it->second->discipline = &ref_it->second;
		}

		for (std::size_t j = 0; j < it->second->module_instances.size(); ++j)
		{
			std::map<std::string, std::shared_ptr<vams::vpi_module> >::iterator ref_it = r.modules.find(it->second->module_instances[j].module_ref);
			if (ref_it == r.modules.end())
				throw std::runtime_error("Can't instantiate a module that isn't defined: " + it->second->module_instances[j].module_ref);
			it->second->module_instances[j].module = ref_it->second.get();

			if (it->second->module_instances[j].module->module_parameters.size() < it->second->module_instances[j].module_param_args.size())
				throw std::runtime_error("Too many module parameter arguments");

			for (std::size_t k = 0; k < it->second->module_instances[j].module_param_args.size(); ++k)
			{
				if (!it->second->module_instances[j].module_param_args[k]->is_const_expr(true))
					throw std::runtime_error("Module parameter arguments must be constant expressions.");
			}

			for (std::size_t k = 0; k < it->second->module_instances[j].args.size(); ++k)
			{
				resolve_decls(m, it->second->module_instances[j].args[k].get(), true);
			}
		}

		for (std::size_t j = 0; j < it->second->analog_stmts.size(); ++j)
			resolve_decls(m, it->second->analog_stmts[j].get(), false);

		for (std::size_t j = 0; j < m.analog_stmts.size(); ++j)
		{
			contribution_lhs_checker checker;
			vams::visit_stmt(checker, m.analog_stmts[j].get());
		}
	}

	std::set<vams::vpi_module *> top_level_modules = get_top_level_modules(r);
	if (top_level_modules.size() != 1)
	{
		r.top_module = 0;
		for (std::set<vams::vpi_module *>::const_iterator it = top_level_modules.begin(); it != top_level_modules.end(); ++it)
		{
			if ((*it)->name == "main")
			{
				r.top_module = *it;
				break;
			}
		}

		if (!r.top_module)
			throw std::runtime_error("There must be exactly one top-level module!");
	}
	else
	{
		r.top_module = *top_level_modules.begin();
	}
}
