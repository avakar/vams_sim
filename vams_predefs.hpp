// Copyright Martin Vejnar 2012.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef VAMS_SIM_VAMS_PREDEFS_HPP
#define VAMS_SIM_VAMS_PREDEFS_HPP

#include "vams_ast.hpp"

void add_vams_predefs(vams::vpi_root & r);

#endif // VAMS_SIM_VAMS_PREDEFS_HPP
