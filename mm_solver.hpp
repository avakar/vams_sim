// Copyright Martin Vejnar 2012.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef MM_SOLVER_HPP
#define MM_SOLVER_HPP

#include "merged_module.hpp"
#include <nvector/nvector_serial.h>

class mm_solver
{
public:
	mm_solver(merged_module const & mm);
	~mm_solver();

	std::vector<double> get_iter(double t);

private:
	merged_module const & mm;
	void * ida_mem;
	N_Vector y;
	N_Vector ddty;
	int N;
	std::map<vams::vpi_ddt_expr *, std::size_t> m_ddt_var_map;
	std::vector<vams::vpi_ddt_expr *> m_ddt_vars;

	struct expr_evaluator;
	struct stmt_walker;
	static int system_fn(realtype tt, N_Vector yy, N_Vector yp, N_Vector rr, void *user_data);
};

#endif // MM_SOLVER_HPP
