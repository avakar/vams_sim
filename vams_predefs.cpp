// Copyright Martin Vejnar 2012.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#include "vams_predefs.hpp"
using namespace vams;

void add_vams_predefs(vams::vpi_root & r)
{
	vpi_nature & current_nature = r.natures["Current"];
	current_nature.name = "Current";
	current_nature.units = "A";
	current_nature.access = "I";
	current_nature.idt_nature_name = "Charge";
	current_nature.abstol = 1e-12;

	vpi_nature & charge_nature = r.natures["Charge"];
	charge_nature.name = "Charge";
	charge_nature.units = "coul";
	charge_nature.access = "Q";
	charge_nature.ddt_nature_name = "Current";
	charge_nature.abstol = 1e-14;

	vpi_nature & voltage_nature = r.natures["Voltage"];
	voltage_nature.name = "Voltage";
	voltage_nature.units = "V";
	voltage_nature.access = "V";
	voltage_nature.idt_nature_name = "Flux";
	voltage_nature.abstol = 1e-6;

	vpi_nature & flux_nature = r.natures["Flux"];
	flux_nature.name = "Flux";
	flux_nature.units = "Wb";
	flux_nature.access = "Phi";
	flux_nature.ddt_nature_name = "Voltage";
	flux_nature.abstol = 1e-9;

	vpi_discipline & electrical_disc = r.disciplines["electrical"];
	electrical_disc.name = "electrical";
	electrical_disc.potential_name = "Voltage";
	electrical_disc.flow_name = "Current";

	vpi_discipline & voltage_disc = r.disciplines["voltage"];
	electrical_disc.name = "voltage";
	electrical_disc.potential_name = "Voltage";

	vpi_discipline & current_disc = r.disciplines["current"];
	electrical_disc.name = "current";
	electrical_disc.flow_name = "Current";
}
