// Copyright Martin Vejnar 2012.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#include "merged_module.hpp"
#include "vams_instantiate.hpp"
#include <ostream>

struct branch_detector
	: vams::default_visitor<branch_detector, void>
{
	void operator()(vams::vpi_contribution_stmt * ee)
	{
		vams::vpi_function_call_expr & fncall = dynamic_cast<vams::vpi_function_call_expr &>(*ee->lhs);
		branch_kinds[fncall.signal_access->branch] = fncall.signal_access->type;
	}

	using default_visitor::operator();

	std::map<vams::vpi_branch *, vams::vpi_access_function_call::access_type> branch_kinds;
};


static vams::vpi_branch * clone_branch(
	merged_module & mm,
	std::string const & name_prefix,
	std::map<vams::vpi_net *, vams::vpi_net *> & net_map,
	std::map<vams::vpi_branch *, vams::vpi_branch *> & branch_map,
	vams::vpi_branch * source)
{
	vams::vpi_branch * & mm_branch = branch_map[source];
	if (!mm_branch)
	{
		std::shared_ptr<vams::vpi_branch> p(new vams::vpi_branch());
		p->name = name_prefix + "." + source->name;
		if (source->lhs_net)
			p->lhs_net = net_map[source->lhs_net];
		else
			p->lhs_net = mm.ground_net;
		if (source->rhs_net)
			p->rhs_net = net_map[source->rhs_net];
		else
			p->rhs_net = mm.ground_net;
		p->discipline = source->discipline;
		mm.branches.push_back(p);
		mm_branch = p.get();
	}
	return mm_branch;
}

static void bmm_impl(std::string const & name_prefix,
	std::map<vams::vpi_net *, vams::vpi_net *> & net_map,
	std::map<vams::vpi_branch *, vams::vpi_branch *> & branch_map,
	std::map<vams::vpi_parameter *, vams::vpi_value> & mod_param_assignment,
	merged_module & mm, vams::vpi_module * m)
{
	for (std::map<std::string, std::shared_ptr<vams::vpi_net> >::iterator it = m->nets.begin(); it != m->nets.end(); ++it)
	{
		vams::vpi_net * & mm_net = net_map[it->second.get()];
		if (!mm_net)
		{
			if (it->second->net_type == vams::nt_ground)
			{
				mm_net = mm.ground_net;
			}
			else
			{
				std::shared_ptr<vams::vpi_net> p(new vams::vpi_net());
				p->name = name_prefix + "." + it->second->name;
				p->net_type = it->second->net_type;
				mm.nets.push_back(p);
				mm_net = p.get();
			}
		}
	}

	for (std::map<std::string, std::shared_ptr<vams::vpi_branch> >::iterator it = m->named_branches.begin(); it != m->named_branches.end(); ++it)
		clone_branch(mm, name_prefix, net_map, branch_map, it->second.get());

	for (std::map<std::pair<vams::vpi_net *, vams::vpi_net *>, std::shared_ptr<vams::vpi_branch> >::iterator it = m->unnamed_branches.begin(); it != m->unnamed_branches.end(); ++it)
		clone_branch(mm, name_prefix, net_map, branch_map, it->second.get());

	for (size_t i = 0; i < m->analog_stmts.size(); ++i)
	{
		mm.stmts.push_back(instantiate_stmt(net_map, branch_map, mod_param_assignment, m->analog_stmts[i].get()));
	}

	for (size_t i = 0; i < m->module_instances.size(); ++i)
	{
		vams::vpi_module * instm = m->module_instances[i].module;

		std::map<vams::vpi_net *, vams::vpi_net *> inst_net_map;
		for (size_t j = 0; j < instm->ports.size(); ++j)
		{
			vams::vpi_net * & n = inst_net_map[instm->ports[j].net];
			// XXX
			n = net_map[dynamic_cast<vams::vpi_decl_ref_expr *>(m->module_instances[i].args[j].get())->item.net];
		}

		std::map<vams::vpi_parameter *, vams::vpi_value> inst_param_assignment;
		for (size_t j = 0; j < m->module_instances[i].module_param_args.size(); ++j)
		{
			vams::vpi_expr * e = m->module_instances[i].module_param_args[j].get();
			inst_param_assignment[&instm->module_parameters[j]] = e->get_const_value();
		}

		for (size_t j = m->module_instances[i].module_param_args.size(); j < instm->module_parameters.size(); ++j)
		{
			inst_param_assignment[&instm->module_parameters[j]] = instm->module_parameters[j].default_value_expr->get_const_value();
		}

		std::map<vams::vpi_branch *, vams::vpi_branch *> inst_branch_map;

		bmm_impl(
			name_prefix + "." + m->module_instances[i].name,
			inst_net_map,
			inst_branch_map,
			inst_param_assignment,
			mm,
			instm);
	}
}

void build_merged_module(merged_module & mm, vams::vpi_root & r)
{
	std::map<vams::vpi_net *, vams::vpi_net *> net_map;

	std::shared_ptr<vams::vpi_net> gnd(new vams::vpi_net());
	gnd->net_type = vams::nt_ground;
	gnd->name = "gnd";
	mm.nets.push_back(gnd);
	mm.ground_net = gnd.get();

	std::map<vams::vpi_branch *, vams::vpi_branch *> branch_map;
	std::map<vams::vpi_parameter *, vams::vpi_value> mod_param_assignment;
	bmm_impl("", net_map, branch_map, mod_param_assignment, mm, r.top_module);

	mm.calc_indexes();
}

void merged_module::calc_indexes()
{
	net_indexes.clear();
	for (std::size_t i = 0; i < nets.size(); ++i)
		net_indexes[nets[i].get()] = i;

	branch_indexes.clear();
	for (std::size_t i = 0; i < branches.size(); ++i)
		branch_indexes[branches[i].get()] = i + nets.size();

	branch_detector bd;
	for (std::size_t i = 0; i < stmts.size(); ++i)
		vams::visit_stmt(bd, stmts[i].get());

	branch_kinds = bd.branch_kinds;
}

struct stmt_printer
{
	typedef void return_type;

	stmt_printer(std::ostream & o, merged_module const & mm)
		: o(o), mm(mm)
	{
	}

	void operator()(vams::vpi_null_stmt * ee)
	{
	}

	void operator()(vams::vpi_analog_stmt * ee)
	{
		o << "analog ";
		visit_stmt(*this, ee->nested.get());
		o << ";\n";
	}

	void operator()(vams::vpi_compound_stmt * ee)
	{
		o << "begin ";
		for (size_t i = 0; i < ee->stmts.size(); ++i)
			visit_stmt(*this, ee->stmts[i].get());
		o << "end ";
	}

	void operator()(vams::vpi_contribution_stmt * ee)
	{
		visit_expr(*this, ee->lhs.get());
		o << " <+ ";
		visit_expr(*this, ee->rhs.get());
	}

	void operator()(vams::vpi_constant * ee)
	{
		switch (ee->value.kind())
		{
		case vams::vpi_value::k_int:
			o << ee->value.get_int();
			break;
		case vams::vpi_value::k_real:
			o << ee->value.get_real();
			break;
		case vams::vpi_value::k_str:
			o << "\"" << ee->value.get_string() << "\"";
			break;
		}
	}

	void operator()(vams::vpi_unary_expr * ee)
	{
		// XXX
	}

	void operator()(vams::vpi_binary_expr * ee)
	{
		visit_expr(*this, ee->lhs.get());
		switch (ee->op)
		{
		case vams::vpi_binary_expr::op_add:
			o << " + ";
			break;
		case vams::vpi_binary_expr::op_sub:
			o << " - ";
			break;
		case vams::vpi_binary_expr::op_mul:
			o << " * ";
			break;
		case vams::vpi_binary_expr::op_div:
			o << " / ";
			break;
		case vams::vpi_binary_expr::op_mod:
			o << " % ";
			break;
		}
		visit_expr(*this, ee->rhs.get());
	}

	void operator()(vams::vpi_cond_expr * ee)
	{
		visit_expr(*this, ee->cond.get());
		o << "? ";
		visit_expr(*this, ee->lhs.get());
		o << ": ";
		visit_expr(*this, ee->rhs.get());
	}

	void operator()(vams::vpi_paren_expr * ee)
	{
		o << "(";
		visit_expr(*this, ee->nested.get());
		o << ")";
	}

	void operator()(vams::vpi_function_call_expr * ee)
	{
		o << ee->function_name << "(";
		for (size_t i = 0; i < ee->arguments.size(); ++i)
		{
			if (i != 0)
				o << ", ";
			visit_expr(*this, ee->arguments[i].get());
		}
		o << ")";
	}

	void operator()(vams::vpi_ddt_expr * ee)
	{
		o << "ddt(";
		visit_expr(*this, ee->nested.get());
		o << ")";
	}

	void operator()(vams::vpi_port_probe_expr * ee)
	{
		o << ee->accessor_name << "(" << ee->probe_ref << ")";
	}

	void operator()(vams::vpi_decl_ref_expr * ee)
	{
		if (ee->item.kind == vams::si_net)
			o << mm.nets[mm.net_indexes.find(ee->item.net)->second]->name;
		else
			o << ee->decl_name;
	}

	std::ostream & o;
	merged_module const & mm;
};

void merged_module::print(std::ostream & o) const
{
	for (auto const & stmt : this->stmts)
	{
		stmt_printer p(o, *this);
		vams::visit_stmt(p, stmt.get());
	}
}
