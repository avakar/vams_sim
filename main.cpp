// Copyright Martin Vejnar 2012.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#include "vams_parser.hpp"
#include "vams_sema.hpp"
#include "merged_module.hpp"
#include "mm_solver.hpp"
#include "vams_predefs.hpp"

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <iomanip>

vams::vpi_root parse_va_file(std::string const & full_path)
{
	std::filebuf fin;
	fin.open(full_path.c_str(), std::ios::in);
	if (!fin.is_open())
		throw std::runtime_error("error: Failed to open input file: " + full_path);

	parser p;

	char buf[1024];
	for (;;)
	{
		std::streamsize read = fin.sgetn(buf, 1024);
		if (read == 0)
			break;
		p.push_data(buf, buf + read);
	}

	return p.finish();
}

void run_dc_analysis(merged_module const & mm)
{
	for (std::size_t i = 1; i < mm.nets.size(); ++i)
		std::cout << std::setw(12) << ("V(" + mm.nets[i]->name + ")") << " ";
	for (std::size_t i = 0; i < mm.branches.size(); ++i)
		std::cout << std::setw(12) << ("I(" + mm.branches[i]->lhs_net->name + ", " + mm.branches[i]->rhs_net->name + ")") << " ";
	std::cout << "\n";

	mm_solver solver(mm);

	for (int i = 1; i < 1000; ++i)
	{
		std::vector<double> res = solver.get_iter(0.001*i);
		for (size_t i = 1; i < mm.nets.size() + mm.branches.size(); ++i)
			std::cout << std::fixed << std::setw(12) << std::setprecision(6) << res[i] << " ";
		std::cout << "\n";
	}
}

int main(int argc, char * argv[])
{
	std::vector<char const *> fnames;
	enum class mode_t { print_mm, dc_analysis } mode = mode_t::dc_analysis;

	for (int i = 1; i < argc; ++i)
	{
		if (strcmp(argv[i], "--print-mm") == 0)
			mode = mode_t::print_mm;
		else
			fnames.push_back(argv[i]);
	}

	if (fnames.empty())
	{
		std::cout << "Usage: " << argv[0] << " <filename.va>...\n";
		return 1;
	}

	try
	{
		vams::vpi_root r;
		add_vams_predefs(r);

		for (auto fname: fnames)
			r.append(parse_va_file(fname));
		vams_sema(r);

		merged_module mm;
		build_merged_module(mm, r);

		switch (mode)
		{
		case mode_t::print_mm:
			mm.print(std::cout);
			break;
		case mode_t::dc_analysis:
			run_dc_analysis(mm);
			break;
		}
	}
	catch (std::runtime_error const & e)
	{
		std::cerr << "error: " << e.what() << "\n";
		return 2;
	}

	return 0;
}
