// Copyright Martin Vejnar 2012.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef SEMA_HPP
#define SEMA_HPP

#include "vams_ast.hpp"

void vams_sema(vams::vpi_root & r);

#endif // SEMA_HPP
