// Copyright Martin Vejnar 2012.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#include "vams_instantiate.hpp"

struct expr_instantiator
{
	typedef std::shared_ptr<vams::vpi_expr> return_type;

	expr_instantiator(
		std::map<vams::vpi_net *, vams::vpi_net *> const & net_map,
		std::map<vams::vpi_branch *, vams::vpi_branch *> const & branch_map,
		std::map<vams::vpi_parameter *, vams::vpi_value> const & param_map)
		: net_map(net_map), branch_map(branch_map), param_map(param_map)
	{
	}

	return_type operator()(vams::vpi_constant * ee)
	{
		std::shared_ptr<vams::vpi_constant> rr(new vams::vpi_constant());
		rr->value = ee->value;
		return rr;
	}

	return_type operator()(vams::vpi_unary_expr * ee)
	{
		std::shared_ptr<vams::vpi_unary_expr> rr(new vams::vpi_unary_expr());
		rr->op = ee->op;
		rr->arg = instantiate_expr(net_map, branch_map, param_map, ee->arg.get());
		return rr;
	}

	return_type operator()(vams::vpi_binary_expr * ee)
	{
		std::shared_ptr<vams::vpi_binary_expr> rr(new vams::vpi_binary_expr());
		rr->op = ee->op;
		rr->lhs = instantiate_expr(net_map, branch_map, param_map, ee->lhs.get());
		rr->rhs = instantiate_expr(net_map, branch_map, param_map, ee->rhs.get());
		return rr;
	}

	return_type operator()(vams::vpi_cond_expr * ee)
	{
		std::shared_ptr<vams::vpi_cond_expr> rr(new vams::vpi_cond_expr());
		rr->cond = instantiate_expr(net_map, branch_map, param_map, ee->cond.get());
		rr->lhs = instantiate_expr(net_map, branch_map, param_map, ee->lhs.get());
		rr->rhs = instantiate_expr(net_map, branch_map, param_map, ee->rhs.get());
		return rr;
	}

	return_type operator()(vams::vpi_paren_expr * ee)
	{
		return instantiate_expr(net_map, branch_map, param_map, ee->nested.get());
	}

	return_type operator()(vams::vpi_ddt_expr * ee)
	{
		std::shared_ptr<vams::vpi_ddt_expr> rr(new vams::vpi_ddt_expr());
		rr->nested = instantiate_expr(net_map, branch_map, param_map, ee->nested.get());
		return rr;
	}

	return_type operator()(vams::vpi_function_call_expr * ee)
	{
		std::shared_ptr<vams::vpi_function_call_expr> rr(new vams::vpi_function_call_expr());
		rr->function_name = ee->function_name;
		for (size_t i = 0; i < ee->arguments.size(); ++i)
			rr->arguments.push_back(instantiate_expr(net_map, branch_map, param_map, ee->arguments[i].get()));
		if (ee->signal_access)
		{
			rr->signal_access.reset(new vams::vpi_access_function_call());
			rr->signal_access->type = ee->signal_access->type;
			rr->signal_access->discipline = ee->signal_access->discipline;
			rr->signal_access->nature = ee->signal_access->nature;
			rr->signal_access->branch = branch_map.find(ee->signal_access->branch)->second;
			rr->signal_access->reverse_access = ee->signal_access->reverse_access;
		}
		return rr;
	}

	return_type operator()(vams::vpi_port_probe_expr * ee)
	{
		std::shared_ptr<vams::vpi_port_probe_expr> rr(new vams::vpi_port_probe_expr());
		rr->accessor_name = ee->accessor_name;
		rr->probe_ref = ee->probe_ref;
		return rr;
	}

	return_type operator()(vams::vpi_decl_ref_expr * ee)
	{
		if (ee->item.kind == vams::si_parameter)
		{
			std::shared_ptr<vams::vpi_constant> rr(new vams::vpi_constant());
			rr->value = param_map.find(ee->item.param)->second;
			return rr;
		}

		std::shared_ptr<vams::vpi_decl_ref_expr> rr(new vams::vpi_decl_ref_expr());
		rr->decl_name = ee->decl_name;
		if (ee->item.kind == vams::si_net)
			rr->item = net_map.find(ee->item.net)->second;
		if (ee->item.kind == vams::si_branch)
			rr->item = branch_map.find(ee->item.branch)->second;
		return rr;
	}

	std::map<vams::vpi_net *, vams::vpi_net *> const & net_map;
	std::map<vams::vpi_branch *, vams::vpi_branch *> const & branch_map;
	std::map<vams::vpi_parameter *, vams::vpi_value> const & param_map;
};

std::shared_ptr<vams::vpi_expr> instantiate_expr(
	std::map<vams::vpi_net *, vams::vpi_net *> const & net_map,
	std::map<vams::vpi_branch *, vams::vpi_branch *> const & branch_map,
	std::map<vams::vpi_parameter *, vams::vpi_value> const & param_map,
	vams::vpi_expr * expr)
{
	expr_instantiator visitor(net_map, branch_map, param_map);
	return vams::visit_expr(visitor, expr);
}

struct stmt_instantiator
{
	typedef std::shared_ptr<vams::vpi_stmt> return_type;

	stmt_instantiator(
		std::map<vams::vpi_net *, vams::vpi_net *> const & net_map,
		std::map<vams::vpi_branch *, vams::vpi_branch *> const & branch_map,
		std::map<vams::vpi_parameter *, vams::vpi_value> const & param_map)
		: net_map(net_map), branch_map(branch_map), param_map(param_map)
	{
	}

	return_type operator()(vams::vpi_null_stmt * ss)
	{
		return std::shared_ptr<vams::vpi_null_stmt>(new vams::vpi_null_stmt());
	}

	return_type operator()(vams::vpi_analog_stmt * ss)
	{
		std::shared_ptr<vams::vpi_analog_stmt> rr(new vams::vpi_analog_stmt());
		rr->initial = ss->initial;
		rr->nested = instantiate_stmt(net_map, branch_map, param_map, ss->nested.get());
		return rr;
	}

	return_type operator()(vams::vpi_compound_stmt * ss)
	{
		std::shared_ptr<vams::vpi_compound_stmt> rr(new vams::vpi_compound_stmt());
		for (size_t i = 0; i < ss->stmts.size(); ++i)
		{
			rr->stmts.push_back(instantiate_stmt(net_map, branch_map, param_map, ss->stmts[i].get()));
		}
		return rr;
	}

	return_type operator()(vams::vpi_contribution_stmt * ss)
	{
		std::shared_ptr<vams::vpi_contribution_stmt> rr(new vams::vpi_contribution_stmt());
		rr->lhs = instantiate_expr(net_map, branch_map, param_map, ss->lhs.get());
		rr->rhs = instantiate_expr(net_map, branch_map, param_map, ss->rhs.get());
		return rr;
	}

	std::map<vams::vpi_net *, vams::vpi_net *> const & net_map;
	std::map<vams::vpi_branch *, vams::vpi_branch *> const & branch_map;
	std::map<vams::vpi_parameter *, vams::vpi_value> const & param_map;
};

std::shared_ptr<vams::vpi_stmt> instantiate_stmt(
	std::map<vams::vpi_net *, vams::vpi_net *> const & net_map,
	std::map<vams::vpi_branch *, vams::vpi_branch *> const & branch_map,
	std::map<vams::vpi_parameter *, vams::vpi_value> const & param_map,
	vams::vpi_stmt * stmt)
{
	stmt_instantiator visitor(net_map, branch_map, param_map);
	return vams::visit_stmt(visitor, stmt);
}
