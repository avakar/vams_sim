# Copyright Martin Vejn�r 2012.
# Distributed under the Boost Software License, Version 1.0.
# (See accompanying file LICENSE.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

%include {
	#include "vams_ast.hpp"
	#include <stdint.h>
	#include <memory>
	#include <string>
}

%discard {\s+}
%discard {//[^\n]*\n?}
%discard {/\*([^\*]|\*+[^/\*])*\*+/}

IDENT :: {std::string}
IDENT ::= {[a-zA-Z_$][a-zA-Z0-9_$]*}.

%test IDENT ::= "_test".
%test vams_root ::= nature_declaration.

NUM :: {uint32_t}
NUM ::= {[0-9]+}(s). { return boost::lexical_cast<uint32_t>(s); }

REAL :: {double}
REAL ::= {[0-9]+(\.[0-9]+|(\.[0-9]+|)[eE][\+\-][0-9]+)}(s). { return std::stod(s); }

STR :: {std::string}
STR ::= {"[A-Za-z]*"}(s). { return s.substr(1, s.size() - 2); }

vams_root :: {vams::vpi_root}
%root vams_root(r) ::= . {}
vams_root(r) ::= vams_root(r) nature_declaration(n). { r.natures[n.name] = n; }
vams_root(r) ::= vams_root(r) discipline_declaration(n). { r.disciplines[n.name] = n; }
vams_root(r) ::= vams_root(r) module_declaration(n). { r.modules[n->name] = n; }

nature_declaration :: {vams::vpi_nature}
nature_declaration ::= open_nature_declaration "endnature".

open_nature_declaration :: {vams::vpi_nature}
open_nature_declaration(r) ::= "nature" IDENT(name). { r.name = name; }
open_nature_declaration(r) ::= "nature" IDENT(name) ";". { r.name = name; }
open_nature_declaration(r) ::= "nature" IDENT(name) ":" IDENT(parent). { r.name = name; r.parent_name = parent; }
open_nature_declaration(r) ::= "nature" IDENT(name) ":" IDENT(parent) ";". { r.name = name; r.parent_name = parent; }

open_nature_declaration(r) ::= open_nature_declaration(r) IDENT(attr_name) "=" expr(e) ";". {
	r.user_attributes[attr_name] = e->get_const_value();
}

open_nature_declaration(r) ::= open_nature_declaration(r) "abstol" "=" expr(e) ";". {
	r.abstol = e->get_const_value().get_real();
}

open_nature_declaration(r) ::= open_nature_declaration(r) "access" "=" IDENT(e) ";". {
	r.access = e;
}

open_nature_declaration(r) ::= open_nature_declaration(r) "ddt_nature" "=" IDENT(e) ";". {
	r.ddt_nature_name = e;
}

open_nature_declaration(r) ::= open_nature_declaration(r) "idt_nature" "=" IDENT(e) ";". {
	r.idt_nature_name = e;
}

open_nature_declaration(r) ::= open_nature_declaration(r) "units" "=" STR(e) ";". {
	r.units = e;
}

discipline_declaration :: {vams::vpi_discipline}
discipline_declaration ::= open_discipline_declaration "enddiscipline".

open_discipline_declaration :: {vams::vpi_discipline}
open_discipline_declaration(r) ::= "discipline" IDENT(name). { r.name = name; }
open_discipline_declaration(r) ::= "discipline" IDENT(name) ";". { r.name = name; }

open_discipline_declaration(r) ::= open_discipline_declaration(r) "potential" IDENT(name) ";". {
	r.potential_name = name;
}

open_discipline_declaration(r) ::= open_discipline_declaration(r) "flow" IDENT(name) ";". {
	r.flow_name = name;
}

open_discipline_declaration(r) ::= open_discipline_declaration(r) "domain" "discrete" ";". {
	// TODO
}

open_discipline_declaration(r) ::= open_discipline_declaration(r) "domain" "continuous" ";". {
	// TODO
}

module_keyword ::= "module".
module_keyword ::= "connectmodule".

module_declaration :: {std::shared_ptr<vams::vpi_module>}
module_declaration ::= open_module_declaration "endmodule".

open_module_declaration :: {std::shared_ptr<vams::vpi_module>}
open_module_declaration(r) ::= module_keyword IDENT(name) opt_module_parameter_port_list(mod_params) list_of_ports(ports) ";". {
	// TODO: exception safety?
	r.reset(new vams::vpi_module());
	r->name = name;
	r->ports.swap(ports);
	r->module_parameters.swap(mod_params);
}

open_module_declaration(r) ::= open_module_declaration(r) analog_construct(a). {
	r->analog_stmts.push_back(a);
}

open_module_declaration(r) ::= open_module_declaration(r) module_instantiation(a). {
	r->module_instances.push_back(a);
}

open_module_declaration(r) ::= open_module_declaration(r) net_declaration(n). {
	for (std::size_t i = 0; i < n.size(); ++i)
		r->nets[n[i]->name] = n[i];
}

net_declaration :: {std::vector<std::shared_ptr<vams::vpi_net> >}
net_declaration(r) ::= IDENT(discipline_name) net_ident_list(l) ";". {
	for (std::size_t i = 0; i < l.size(); ++i)
	{
		std::shared_ptr<vams::vpi_net> n(new vams::vpi_net);
		n->discipline_name = discipline_name;
		n->name = l[i];
		r.push_back(n);
	}
}

net_declaration(r) ::= net_type(t) IDENT(discipline_name) net_ident_list(l) ";". {
	for (std::size_t i = 0; i < l.size(); ++i)
	{
		std::shared_ptr<vams::vpi_net> n(new vams::vpi_net);
		n->net_type = t;
		n->discipline_name = discipline_name;
		n->name = l[i];
		r.push_back(n);
	}
}

net_declaration(r) ::= net_type(t) net_ident_list(l) ";". {
	for (std::size_t i = 0; i < l.size(); ++i)
	{
		std::shared_ptr<vams::vpi_net> n(new vams::vpi_net);
		n->net_type = t;
		n->name = l[i];
		r.push_back(n);
	}
}

net_type :: {vams::net_type_t}
net_type ::= "ground". { return vams::nt_ground; }

branch_declaration :: {std::vector<std::shared_ptr<vams::vpi_branch> >}
branch_declaration(r) ::= "branch" "(" IDENT(lhs_net) ")" net_ident_list(l). {
	for (std::size_t i = 0; i < l.size(); ++i)
	{
		std::shared_ptr<vams::vpi_branch> b(new vams::vpi_branch());
		b->name = l[i];
		b->lhs_net_name = lhs_net;
		r.push_back(b);
	}
}

branch_declaration(r) ::= "branch" "(" IDENT(lhs_net) "," IDENT(rhs_net) ")" net_ident_list(l). {
	for (std::size_t i = 0; i < l.size(); ++i)
	{
		std::shared_ptr<vams::vpi_branch> b(new vams::vpi_branch());
		b->name = l[i];
		b->lhs_net_name = lhs_net;
		b->rhs_net_name = rhs_net;
		r.push_back(b);
	}
}

net_ident_list :: {std::vector<std::string>}
net_ident_list(r) ::= IDENT(name). {
	r.push_back(name);
}
net_ident_list(r) ::= net_ident_list(r) "," IDENT(name). {
	r.push_back(name);
}

module_instantiation :: {vams::vpi_module_instance}
module_instantiation(r) ::= IDENT(module_ref) IDENT(name) "(" expr_list(args) ")" ";". {
	r.module_ref = module_ref;
	r.name = name;
	r.args.swap(args);
}

module_instantiation(r) ::= IDENT(module_ref) "#" "(" expr_list(mod_args) ")" IDENT(name) "(" expr_list(args) ")" ";". {
	r.module_ref = module_ref;
	r.name = name;
	r.args.swap(args);
	r.module_param_args.swap(mod_args);
}

expr_list :: {std::vector<std::shared_ptr<vams::vpi_expr> >}
expr_list(r) ::= . {}
expr_list(r) ::= expr(e). { r.push_back(e); }
expr_list(r) ::= expr_list(r) "," expr(e). { r.push_back(e); }

analog_construct :: {std::shared_ptr<vams::vpi_analog_stmt>}
analog_construct(r) ::= "analog" stmt(s). {
	r.reset(new vams::vpi_analog_stmt());
	r->initial = false;
	r->nested = s;
}
analog_construct(r) ::= "analog" "initial" stmt(s). {
	r.reset(new vams::vpi_analog_stmt());
	r->initial = true;
	r->nested = s;
}

stmt :: {std::shared_ptr<vams::vpi_stmt>}
stmt(r) ::= seq_stmt(s). { r = s; }
stmt(r) ::= contribution_stmt(s). { r = s; }

contribution_stmt :: {std::shared_ptr<vams::vpi_contribution_stmt>}
contribution_stmt(r) ::= branch_lvalue(lhs) "<+" expr(rhs) ";". {
	r.reset(new vams::vpi_contribution_stmt());
	r->lhs = lhs;
	r->rhs = rhs;
}

branch_lvalue :: {std::shared_ptr<vams::vpi_expr>}
branch_lvalue ::= function_call.

seq_stmt :: {std::shared_ptr<vams::vpi_compound_stmt>}
seq_stmt ::= open_seq_stmt "end".

open_seq_stmt :: {std::shared_ptr<vams::vpi_compound_stmt>}
open_seq_stmt(r) ::= "begin". {
	r.reset(new vams::vpi_compound_stmt());
}
open_seq_stmt(r) ::= open_seq_stmt(r) stmt(s). {
	r->stmts.push_back(s);
}

opt_module_parameter_port_list :: {std::vector<vams::vpi_parameter>}
opt_module_parameter_port_list(r) ::= . {}
opt_module_parameter_port_list(r) ::= "#" "(" ")". {}
opt_module_parameter_port_list ::= open_module_parameter_port_list ")".

open_module_parameter_port_list :: {std::vector<vams::vpi_parameter>}
open_module_parameter_port_list(r) ::= "#" "(" parameter_declaration(param). { r.push_back(param); }
open_module_parameter_port_list(r) ::= open_module_parameter_port_list(r) "," parameter_declaration(param). { r.push_back(param); }

parameter_declaration :: {vams::vpi_parameter}
parameter_declaration(r) ::= "parameter" parameter_type(type) IDENT(name) "=" expr(e). {
	r.name = name;
	r.type = type;
	r.default_value_expr.swap(e);
}

parameter_type :: {vams::variable_type}
parameter_type ::= "integer". { return vams::vt_integer; }
parameter_type ::= "real". { return vams::vt_real; }
parameter_type ::= "realtime". { return vams::vt_realtime; }
parameter_type ::= "time". { return vams::vt_time; }
parameter_type ::= "string". { return vams::vt_string; }

list_of_ports :: {std::vector<vams::vpi_port>}
list_of_ports(r) ::= . {}
list_of_ports(r) ::= "(" ")". {}
list_of_ports ::= open_list_of_ports ")".

open_list_of_ports :: {std::vector<vams::vpi_port>}
open_list_of_ports(l) ::= "(" port(p). { l.push_back(p); }
open_list_of_ports(l) ::= open_list_of_ports(l) "," port(p). { l.push_back(p); }

port :: {vams::vpi_port}
port(r) ::= IDENT(name). {
	r.name = name;
	r.direction = -1;
}

expr :: {std::shared_ptr<vams::vpi_expr>}
expr ::= add_expr.

add_expr_op :: {vams::vpi_binary_expr::op_kind}
add_expr_op ::= "+". { return vams::vpi_binary_expr::op_add; }
add_expr_op ::= "-". { return vams::vpi_binary_expr::op_sub; }

mul_expr_op :: {vams::vpi_binary_expr::op_kind}
mul_expr_op ::= "*". { return vams::vpi_binary_expr::op_mul; }
mul_expr_op ::= "/". { return vams::vpi_binary_expr::op_div; }
mul_expr_op ::= "%". { return vams::vpi_binary_expr::op_mod; }

add_expr :: {std::shared_ptr<vams::vpi_expr>}
add_expr ::= mul_expr.
add_expr ::= add_expr(lhs) add_expr_op(op) mul_expr(rhs). {
	std::shared_ptr<vams::vpi_binary_expr> r(new vams::vpi_binary_expr());
	r->op = op;
	r->lhs = lhs;
	r->rhs = rhs;
	return r;
}

mul_expr :: {std::shared_ptr<vams::vpi_expr>}
mul_expr ::= primary.
mul_expr ::= mul_expr(lhs) mul_expr_op(op) primary(rhs). {
	std::shared_ptr<vams::vpi_binary_expr> r(new vams::vpi_binary_expr());
	r->op = op;
	r->lhs = lhs;
	r->rhs = rhs;
	return r;
}

primary :: {std::shared_ptr<vams::vpi_expr>}
primary ::= NUM(n). {
	std::shared_ptr<vams::vpi_constant> r(new vams::vpi_constant());
	r->value = n;
	return r;
}

primary ::= REAL(n). {
	std::shared_ptr<vams::vpi_constant> r(new vams::vpi_constant());
	r->value = n;
	return r;
}

primary ::= IDENT(n). {
	std::shared_ptr<vams::vpi_decl_ref_expr> r(new vams::vpi_decl_ref_expr());
	r->decl_name = n;
	return r;
}

primary ::= "(" expr(e) ")". {
	std::shared_ptr<vams::vpi_paren_expr> r(new vams::vpi_paren_expr());
	r->nested = e;
	return r;
}

primary ::= "ddt" "(" expr(e) ")". {
	std::shared_ptr<vams::vpi_ddt_expr> r(new vams::vpi_ddt_expr());
	r->nested = e;
	return r;
}

primary ::= function_call.

function_call :: {std::shared_ptr<vams::vpi_expr>}
function_call ::= IDENT(fnname) "(" ")". {
	std::shared_ptr<vams::vpi_function_call_expr> r(new vams::vpi_function_call_expr());
	r->function_name = fnname;
	return r;
}

function_call ::= open_function_call ")".

open_function_call :: {std::shared_ptr<vams::vpi_expr>}
open_function_call ::= IDENT(fnname) "(" expr(param). {
	std::shared_ptr<vams::vpi_function_call_expr> r(new vams::vpi_function_call_expr());
	r->function_name = fnname;
	static_cast<vams::vpi_function_call_expr &>(*r).arguments.push_back(param);
	return r;
}
open_function_call(r) ::= open_function_call(r) "," expr(param). {
	static_cast<vams::vpi_function_call_expr &>(*r).arguments.push_back(param);
}
