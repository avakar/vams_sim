// samle_hold model:
//      The sampling model is triggered by the clock. At every falling edge
//      of the clock the input voltage is measured and stored in an intermediate
//      variable. As long the clock is zero, the measured value is applied to
//      the output node. If the clock goes to high, the voltage at the input
//      node is passed through to the output node.

module sample_hold(in,out,clk);
 input in,clk;
 output out;
 voltage in,out,clk;

 parameter real slewrate = 1.0e-9 from (0:inf);
 parameter real clk_vth = 2.5;

 real v;

 analog
 begin
   if (analysis("static") || (V(clk) > clk_vth))
     v = V(in);                         // passing phase

   @(cross(V(clk)-clk_vth,-1))
     v = V(in);                         // sampling phase

   V(out) <+ slew(v,slewrate);
 end
endmodule

