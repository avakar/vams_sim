// lamp with radiation and heating effects


//      p1,p2           electrical pins
//      C_rad           Radiation constant
//      C_tempcap       thermal capacitance
//      Alpha           resistor temperature dependency
//      Rnom            nominal resistance @ 27 Celcius

//      Temp(t1)
/*      ---------------------------------
        |               |               |
        I               C               R
        |               |               |
        -               -               -       $temperature
        Pelectric       Pthermcap       Pradiation
*/

module lamp(p1,p2);
 inout p1,p2;
 electrical p1,p2;

 parameter real C_rad    =1.0e-4,
                C_tempcap=1.0e-12,
                Alpha    =3.0e-3,
                Rnom     =50.0 from (0:inf),
                RnomReftemp=27 from [-`P_CELSIUS0:inf);

 thermal t1;
 branch (p1,p2) res;

 analog
   begin
     reff = Rnom*(1+Alpha*(Temp(t1)-(`P_CELSIUS0+RnomReftemp));

     Pelectric  = pow(V(res),2) / reff;
     Pradiation = C_rad * (pow(Temp(t1),4) - pow($temperature,4));
     Pthermcap  = C_tempcap * ddt(Temp(t1)-$temperature);

     I(res) <+ V(res) / reff;
     Temp(t1) : Pelectric - Pradiation - Pthermcap == 0;
   end
endmodule

