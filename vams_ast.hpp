// Copyright Martin Vejnar 2012.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef VAMS_AST_HPP
#define VAMS_AST_HPP

#include <stdint.h>
#include <cassert>
#include <memory>
#include <map>
#include <vector>
#include <string>

namespace vams {

namespace detail {

template <size_t A, size_t B>
struct static_max
{
	static size_t const value = A > B? A: B;
};

}

struct vpi_value
{
	enum kind_t: uint8_t
	{
		k_none,
		k_int,
		k_real,
		k_str
	};

	kind_t m_kind;
	std::aligned_storage<detail::static_max<sizeof(std::string), detail::static_max<sizeof(uint32_t), sizeof(double)>::value>::value>::type m_storage;

	kind_t kind() const
	{
		return m_kind;
	}

	uint32_t get_int() const
	{
		assert(m_kind == k_int);
		return *reinterpret_cast<uint32_t const *>(&m_storage);
	}

	double get_real() const
	{
		assert(m_kind == k_real);
		return *reinterpret_cast<double const *>(&m_storage);
	}
	std::string get_string() const
	{
		assert(m_kind == k_str);
		return *reinterpret_cast<std::string const *>(&m_storage);
	}

	vpi_value()
		: m_kind(k_none)
	{
	}

	~vpi_value()
	{
		this->clear();
	}

	void clear()
	{
		switch (m_kind)
		{
		case k_str:
			reinterpret_cast<std::string *>(&m_storage)->~basic_string();
			break;
		}

		m_kind = k_none;
	}

	vpi_value(vpi_value const & o)
		: m_kind(k_none)
	{
		(*this) = o;
	}

	vpi_value & operator=(vpi_value const & o)
	{
		this->clear();

		switch (o.m_kind)
		{
		case k_int:
			this->make_storage<uint32_t>(*reinterpret_cast<uint32_t const *>(&o.m_storage));
			break;
		case k_real:
			this->make_storage<double>(*reinterpret_cast<double const *>(&o.m_storage));
			break;
		case k_str:
			this->make_storage<std::string>(*reinterpret_cast<std::string const *>(&o.m_storage));
			break;
		}

		m_kind = o.m_kind;
		return *this;
	}

	vpi_value(uint32_t t)
		: m_kind(k_int)
	{
		this->make_storage<uint32_t>(t);
	}

	vpi_value(double t)
		: m_kind(k_real)
	{
		this->make_storage<double>(t);
	}

	vpi_value(std::string const & v)
		: m_kind(k_str)
	{
		this->make_storage<std::string>(v);
	}

	vpi_value(std::string && v)
		: m_kind(k_str)
	{
		this->make_storage<std::string>(std::move(v));
	}

private:
	template <typename T, typename V>
	T & make_storage(V && v)
	{
		return *new(&m_storage) T(std::forward<V>(v));
	}
};

struct vpi_expr
{
	virtual ~vpi_expr() {}

	virtual bool is_const_expr(bool allow_module_params = false) const { return false; }
	virtual vpi_value get_const_value() const { throw std::runtime_error(""); }
};

struct vpi_constant
	: vpi_expr
{
	vpi_value value;

	virtual bool is_const_expr(bool allow_module_params = false) const { return true; }
	virtual vpi_value get_const_value() const { return value; }
};

struct vpi_unary_expr
	: vpi_expr
{
	int op;
	std::shared_ptr<vpi_expr> arg;

	virtual bool is_const_expr(bool allow_module_params = false) const { return arg->is_const_expr(allow_module_params); }
	virtual vpi_value get_const_value() const
	{
		return arg->get_const_value();
	}
};

struct vpi_binary_expr
	: vpi_expr
{
	enum op_kind
	{
		op_add,
		op_sub,
		op_mul,
		op_div,
		op_mod,
	};

	op_kind op;
	std::shared_ptr<vpi_expr> lhs;
	std::shared_ptr<vpi_expr> rhs;

	virtual bool is_const_expr(bool allow_module_params = false) const
	{
		return lhs->is_const_expr(allow_module_params) && rhs->is_const_expr(allow_module_params);
	}

	virtual vpi_value get_const_value() const
	{
		return lhs->get_const_value();
	}
};

struct vpi_cond_expr
	: vpi_expr
{
	std::shared_ptr<vpi_expr> cond;
	std::shared_ptr<vpi_expr> lhs;
	std::shared_ptr<vpi_expr> rhs;

	virtual bool is_const_expr(bool allow_module_params = false) const
	{
		return cond->is_const_expr(allow_module_params) && lhs->is_const_expr(allow_module_params) && rhs->is_const_expr(allow_module_params);
	}

	virtual vpi_value get_const_value() const
	{
		return lhs->get_const_value();
	}
};

struct vpi_paren_expr
	: vpi_expr
{
	std::shared_ptr<vpi_expr> nested;

	virtual bool is_const_expr(bool allow_module_params = false) const
	{
		return nested->is_const_expr(allow_module_params);
	}

	virtual vpi_value get_const_value() const
	{
		return nested->get_const_value();
	}
};

struct vpi_nature
{
	std::string name;
	std::string parent_name;
	vpi_nature * parent;

	double abstol;
	std::string access;
	std::string ddt_nature_name;
	std::string idt_nature_name;
	vpi_nature * ddt_nature;
	vpi_nature * idt_nature;
	std::string units;
	std::map<std::string, vpi_value> user_attributes;

	vpi_nature()
		: parent(0), abstol(0), ddt_nature(0), idt_nature(0)
	{
	}
};

struct vpi_discipline
{
	std::string name;
	std::string potential_name;
	std::string flow_name;
	vpi_nature * potential;
	vpi_nature * flow;

	vpi_discipline()
		: potential(0), flow(0)
	{
	}
};

enum net_type_t
{
	nt_wire,
	nt_ground,
};

struct vpi_net
{
	std::string name;
	std::string discipline_name;
	vpi_discipline * discipline;
	net_type_t net_type;

	vpi_net()
		: discipline(0), net_type(nt_wire)
	{
	}
};

struct vpi_branch
{
	std::string name;

	std::string lhs_net_name;
	std::string rhs_net_name;

	vpi_net * lhs_net;
	vpi_net * rhs_net;

	vpi_discipline * discipline;
};

struct vpi_access_function_call
{
	enum access_type { at_potential, at_flow } type;
	vpi_discipline * discipline;
	vpi_nature * nature;
	vpi_branch * branch;
	bool reverse_access;

	vpi_access_function_call()
		: type(at_potential), discipline(0), nature(0), branch(0)
	{
	}
};

struct vpi_ddt_expr
	: vpi_expr
{
	std::shared_ptr<vpi_expr> nested;
};

struct vpi_function_call_expr
	: vpi_expr
{
	std::string function_name;
	std::vector<std::shared_ptr<vpi_expr> > arguments;
	std::shared_ptr<vpi_access_function_call> signal_access;
};

struct vpi_port_probe_expr
	: vpi_expr
{
	std::string accessor_name;
	std::string probe_ref;
};

struct vpi_stmt
{
	virtual ~vpi_stmt() {}
};

struct vpi_null_stmt
	: vpi_stmt
{
};

struct vpi_analog_stmt
	: vpi_stmt
{
	std::shared_ptr<vpi_stmt> nested;
	bool initial;
};

struct vpi_compound_stmt
	: vpi_stmt
{
	std::vector<std::shared_ptr<vpi_stmt> > stmts;
};

struct vpi_contribution_stmt
	: vpi_stmt
{
	std::shared_ptr<vpi_expr> lhs;
	std::shared_ptr<vpi_expr> rhs;
};

struct vpi_port
{
	std::string name;
	int direction;
	vpi_net * net;

	vpi_port()
		: net(0)
	{
	}
};

enum variable_type { vt_integer, vt_real, vt_time, vt_realtime, vt_string };

struct vpi_parameter
{
	std::string name;
	variable_type type;
	std::shared_ptr<vpi_expr> default_value_expr;
};

enum scope_item_kind { si_none, si_net, si_branch, si_parameter };

struct scope_item_t
{
	scope_item_kind kind;
	union
	{
		vpi_net * net;
		vpi_branch * branch;
		vpi_parameter * param;
	};

	scope_item_t()
		: kind(si_none)
	{
	}

	scope_item_t(vpi_net * net)
		: kind(si_net), net(net)
	{
	}

	scope_item_t(vpi_branch * branch)
		: kind(si_branch), branch(branch)
	{
	}

	scope_item_t(vpi_parameter * param)
		: kind(si_parameter), param(param)
	{
	}
};

struct vpi_decl_ref_expr
	: vpi_expr
{
	std::string decl_name;
	scope_item_t item;

	bool is_const_expr(bool allow_module_params = false) const
	{
		if (!allow_module_params)
			return false;
		return item.kind == si_parameter;
	}
};

struct vpi_module;

struct vpi_module_instance
{
	std::string module_ref;
	vpi_module * module;
	std::string name;
	std::vector<std::shared_ptr<vpi_expr> > module_param_args;
	std::vector<std::shared_ptr<vpi_expr> > args;

	vpi_module_instance()
		: module(0)
	{
	}
};

struct vpi_module
{
	std::string name;
	std::vector<vpi_port> ports;
	std::vector<std::shared_ptr<vpi_analog_stmt> > analog_stmts;
	std::vector<vpi_module_instance> module_instances;
	std::map<std::string, std::shared_ptr<vpi_net> > nets;

	std::map<std::string, std::shared_ptr<vpi_branch> > named_branches;
	std::map<std::pair<vpi_net *, vpi_net *>, std::shared_ptr<vpi_branch> > unnamed_branches;

	std::vector<vpi_parameter> module_parameters;

	std::map<std::string, scope_item_t> scope;

	scope_item_t lookup(std::string const & name) const
	{
		std::map<std::string, scope_item_t>::const_iterator it = scope.find(name);
		if (it == scope.end())
			return scope_item_t();
		return it->second;
	}
};

struct vpi_root
{
	std::map<std::string, vpi_nature> natures;
	std::map<std::string, vpi_discipline> disciplines;
	std::map<std::string, std::shared_ptr<vpi_module> > modules;

	vpi_module * top_module;

	vpi_root()
		: top_module(0)
	{
	}

	void append(vpi_root const & r)
	{
		// TODO: check for redefinitions
		natures.insert(r.natures.begin(), r.natures.end());
		disciplines.insert(r.disciplines.begin(), r.disciplines.end());
		modules.insert(r.modules.begin(), r.modules.end());
	}
};

template <typename Visitor>
typename Visitor::return_type visit_stmt(Visitor & visitor, vpi_stmt * stmt)
{
	if (vams::vpi_null_stmt * ss = dynamic_cast<vams::vpi_null_stmt *>(stmt))
	{
		return visitor(ss);
	}
	else if (vams::vpi_analog_stmt * ss = dynamic_cast<vams::vpi_analog_stmt *>(stmt))
	{
		return visitor(ss);
	}
	else if (vams::vpi_compound_stmt * ss = dynamic_cast<vams::vpi_compound_stmt *>(stmt))
	{
		return visitor(ss);
	}
	else if (vams::vpi_contribution_stmt * ss = dynamic_cast<vams::vpi_contribution_stmt *>(stmt))
	{
		return visitor(ss);
	}
	else
	{
		assert(false);
		throw 0;
	}
}

template <typename Visitor>
typename Visitor::return_type visit_expr(Visitor & visitor, vpi_expr * expr)
{
	if (vams::vpi_constant * ee = dynamic_cast<vams::vpi_constant *>(expr))
	{
		return visitor(ee);
	}
	else if (vams::vpi_unary_expr * ee = dynamic_cast<vams::vpi_unary_expr *>(expr))
	{
		return visitor(ee);
	}
	else if (vams::vpi_binary_expr * ee = dynamic_cast<vams::vpi_binary_expr *>(expr))
	{
		return visitor(ee);
	}
	else if (vams::vpi_cond_expr * ee = dynamic_cast<vams::vpi_cond_expr *>(expr))
	{
		return visitor(ee);
	}
	else if (vams::vpi_paren_expr * ee = dynamic_cast<vams::vpi_paren_expr *>(expr))
	{
		return visitor(ee);
	}
	else if (vams::vpi_function_call_expr * ee = dynamic_cast<vams::vpi_function_call_expr *>(expr))
	{
		return visitor(ee);
	}
	else if (vams::vpi_ddt_expr * ee = dynamic_cast<vams::vpi_ddt_expr *>(expr))
	{
		return visitor(ee);
	}
	else if (vams::vpi_port_probe_expr * ee = dynamic_cast<vams::vpi_port_probe_expr *>(expr))
	{
		return visitor(ee);
	}
	else if (vams::vpi_decl_ref_expr * ee = dynamic_cast<vams::vpi_decl_ref_expr *>(expr))
	{
		return visitor(ee);
	}
	else
	{
		assert(false);
		throw 0;
	}
}

template <typename Derived, typename ReturnType = void>
struct default_visitor
{
	typedef ReturnType return_type;

	return_type operator()(vams::vpi_null_stmt * ee)
	{
	}

	return_type operator()(vams::vpi_analog_stmt * ee)
	{
		return visit_stmt(static_cast<Derived &>(*this), ee->nested.get());
	}

	return_type operator()(vams::vpi_compound_stmt * ee)
	{
		for (size_t i = 0; i < ee->stmts.size(); ++i)
			visit_stmt(static_cast<Derived &>(*this), ee->stmts[i].get());
	}

	return_type operator()(vams::vpi_contribution_stmt * ee)
	{
		visit_expr(static_cast<Derived &>(*this), ee->lhs.get());
		visit_expr(static_cast<Derived &>(*this), ee->rhs.get());
	}

	return_type operator()(vams::vpi_constant * ee)
	{
	}

	return_type operator()(vams::vpi_unary_expr * ee)
	{
		return visit_expr(static_cast<Derived &>(*this), ee->arg.get());
	}

	return_type operator()(vams::vpi_binary_expr * ee)
	{
		visit_expr(static_cast<Derived &>(*this), ee->lhs.get());
		visit_expr(static_cast<Derived &>(*this), ee->rhs.get());
	}

	return_type operator()(vams::vpi_cond_expr * ee)
	{
		visit_expr(static_cast<Derived &>(*this), ee->cond.get());
		visit_expr(static_cast<Derived &>(*this), ee->lhs.get());
		visit_expr(static_cast<Derived &>(*this), ee->rhs.get());
	}

	return_type operator()(vams::vpi_paren_expr * ee)
	{
		return visit_expr(static_cast<Derived &>(*this), ee->nested.get());
	}

	return_type operator()(vams::vpi_function_call_expr * ee)
	{
		for (size_t i = 0; i < ee->arguments.size(); ++i)
			visit_expr(static_cast<Derived &>(*this), ee->arguments[i].get());
	}

	return_type operator()(vams::vpi_ddt_expr * ee)
	{
		return visit_expr(static_cast<Derived &>(*this), ee->nested.get());
	}

	return_type operator()(vams::vpi_port_probe_expr * ee)
	{
	}

	return_type operator()(vams::vpi_decl_ref_expr * ee)
	{
	}
};

}

#endif // VAMS_AST_HPP
