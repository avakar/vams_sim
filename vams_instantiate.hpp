// Copyright Martin Vejnar 2012.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef INSTANTIATE_HPP
#define INSTANTIATE_HPP

#include "vams_ast.hpp"

std::shared_ptr<vams::vpi_expr> instantiate_expr(
	std::map<vams::vpi_net *, vams::vpi_net *> const & net_map,
	std::map<vams::vpi_branch *, vams::vpi_branch *> const & branch_map,
	std::map<vams::vpi_parameter *, vams::vpi_value> const & param_map,
	vams::vpi_expr * expr);

std::shared_ptr<vams::vpi_stmt> instantiate_stmt(
	std::map<vams::vpi_net *, vams::vpi_net *> const & net_map,
	std::map<vams::vpi_branch *, vams::vpi_branch *> const & branch_map,
	std::map<vams::vpi_parameter *, vams::vpi_value> const & param_map,
	vams::vpi_stmt * stmt);

#endif // INSTANTIATE_HPP
