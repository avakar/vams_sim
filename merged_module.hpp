// Copyright Martin Vejnar 2012.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef MERGED_MODULE_HPP
#define MERGED_MODULE_HPP

#include "vams_ast.hpp"
#include <vector>
#include <map>
#include <iosfwd>

struct merged_module
{
	std::vector<std::shared_ptr<vams::vpi_net> > nets;
	std::vector<std::shared_ptr<vams::vpi_stmt> > stmts;
	std::vector<std::shared_ptr<vams::vpi_branch> > branches;
	vams::vpi_net * ground_net;

	std::map<vams::vpi_net *, std::size_t> net_indexes;
	std::map<vams::vpi_branch *, std::size_t> branch_indexes;
	std::map<vams::vpi_branch *, vams::vpi_access_function_call::access_type> branch_kinds;

	merged_module()
		: ground_net(0)
	{
	}

	void calc_indexes();

	std::size_t get_net_index(vams::vpi_net * a) const
	{
		return net_indexes.find(a)->second;
	}

	std::size_t get_branch_index(vams::vpi_branch * a) const
	{
		return branch_indexes.find(a)->second;
	}

	void print(std::ostream & o) const;
};

void build_merged_module(merged_module & mm, vams::vpi_root & r);

#endif // MERGED_MODULE_HPP
