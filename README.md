The simulator uses `limecc` from my [`yapylr` project][1] to compile
the Verilog-AMS grammar to a lexer/parser. Moreover, it uses IDA
solver from the [`sundials` suite][2] to perform the actual simulation.

Once you have the dependencies ready, you can build the package
with CMake.

    $ mkdir _build
    $ cd _build
    $ cmake ..
    $ make

  [1]: http://bitbucket.org/avakar/yapylr
  [2]: https://computation.llnl.gov/casc/sundials/main.html
